# ruomu-table-grid，若木表格小程序组件

若木表格小程序组件介绍：
>&emsp;&emsp; 若木表格小程序组件的由来其实很简单，起因是我在做自己的小程序时，因为需求设计的原因，需要用到表格统计。
而且有表格标题、表格表头、表格数据，下拉加载下一页，分页数据显示，搜索排序，每行的操作，特定列的颜色等
这样的需求，由于在开源组件里找了许久没有合适的table grid组件，所以就自己封装了一套感觉还不错，所以打
算把这个封装的组件单独剥离出来，成立一个单独的小程序第三方组件UI来供，有这样类似需求童鞋使用，和完善它。
所以，它就诞生了。

</br>

为什么叫若木呢？
>&emsp;&emsp; 最初就打算叫table-gird表格组件来着，但是想着最近在看山海经，山海经里有一个上古神树：”若木“。出自《山海经》
海经第十三卷.海内经，原文：”南海之（外）[内] ，黑水青水之间，有木名曰若木，若水出焉。“当然关于若木的寓意有很多，
这里的寓意就是：指文雅、风度、气质、美丽、初心、善解人意之意，放在我们的组件功能里，我希望它UI：清雅，功能：
善解人意。它能满足我们类似这种表格数据展示的需求中，迸发活力，且越来越优秀。同时，我也希望有更多的人来帮助它，
并且完善它。

</br>

目前具有的特点：

* 支持全局关键词搜索功能
* 支持按类型查询，按传入参数排序，按时间段筛选
* 支持标题，且自定义颜色样式等
* 支持分页数据展示，关键字段数据展示
* 支持表格表头
* 支持表格制定字段颜色，特定
* 支持动态传入按钮，按钮展示方式（平铺/下拉列表式），动态调取父容器的函数及传参
* 支持数据下拉加载分页
* 支持悬浮按钮添加、调取父容器函数

## 使用

* 使用[命令行工具](https://github.com/wechat-miniprogram/miniprogram-cli)进行初始化
* 直接从 github 上 clone 下来 进行npm的编译，
> PS：按开发介绍可以产生组件，如果您不会这样或者不想二次开发直接使用，您可以像引入第三方小程序UI组件那样进行npm安装，目前还处于开发阶段，暂时为上传到npm中，请广大童鞋稍等产品的研发。

</br>

> ps:如果您要好的建议或者想帮助它成长您也可以进行开发提交或者提建议，您可以扫描交流群二维码，如需添加请备注。
![Image text](https://gitee.com/lanlancoder/ruomu-table-grid/raw/master/src/assets/wx.jpg)

</br

## 在本地项目已实现的组件雏形
![Image text](https://gitee.com/lanlancoder/ruomu-table-grid/raw/master/src/assets/table.jpg)


## 开发

1. 安装依赖：

```
npm install
```

2. 执行命令：

```
npm run dev
```

默认会在包根目录下生成 miniprogram\_dev 目录，src 中的源代码会被构建并生成到 miniprogram\_dev/components 目录下。如果需要监听文件变化动态构建，则可以执行命令：

```
npm run watch
```

> ps: 如果 minirpogram\_dev 目录下已存在小程序 demo，执行`npm run dev`则不会再将 tools 下的 demo 拷贝到此目录下。而执行`npm run watch`则会监听 tools 目录下的 demo 变动并进行拷贝。

3. 生成的 miniprogram\_dev 目录是一个小程序项目目录，以此目录作为小程序项目目录在开发者工具中打开即可查看自定义组件被使用的效果。

4. 进阶：

* 如果有额外的构建需求，可自行修改 tools 目录中的构建脚本。
* 内置支持 webpack、less 语法、sourcemap 等功能，默认关闭。如若需要可以自行修改 tools/config.js 配置文件中相关配置。
* 内置支持多入口构建，如若需要可自行调整 tools/config.js 配置文件的 entry 字段。
* 默认开启 eslint，可自行调整规则或在 tools/config.js 中注释掉 eslint-loader 行来关闭此功能。

## 目录结构

以下为推荐使用的目录结构，如果有必要开发者也可以自行做一些调整:

```
|--miniprogram_dev // 开发环境构建目录
|--miniprogram_dist // 生产环境构建目录
|--src // 源码
|   |--components // 通用自定义组件
|   |--images // 图片资源
|   |
|   |--xxx.js/xxx.wxml/xxx.json/xxx.wxss // 暴露的 js 模块/自定义组件入口文件
|
|--test // 测试用例
|--tools // 构建相关代码
|   |--demo // demo 小程序目录，开发环境下会被拷贝生成到 miniprogram_dev 目录中
|   |--config.js // 构建相关配置文件
|
|--gulpfile.js
```

> PS：对外暴露的 js 模块/自定义组件请放在 src 目录下，不宜放置在过深的目录。另外新增的暴露模块需要在 tools/config.js 的 entry 字段中补充，不然不会进行构建。

## 其他命令

* 清空 miniprogram_dist 目录：

```
npm run clean
```

* 清空 miniprogam_dev 目录：

```
npm run clean-dev
```

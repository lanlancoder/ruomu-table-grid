// src/ruomu-search.js
Component({
  options: {
    /**
     * 在组件定义时的选项中启用多slot支持
     */
    multipleSlots: true
  },
  /**
   * 组件的属性列表
   */
  properties: {
    /**
     * 搜索框背景颜色
     */
    background: {
      type: String,
      value: '',
    },
    /**
     * 搜索框大小
     */
    size: {
      type: String,
      value: 'normal'
    },
    /**
     * 输入框提示文字颜色
     */
    placeholderColor: {
      type: String,
      value: '#bdbdbd'
    },
    /**
     * 输入框提示文字文案
     */
    placeholder: {
      type: String,
      value: '请输入搜索关键词'
    },
    /**
     * 输入框输入的最大长度
     */
    maxlength: {
      type: Number,
      value: 50
    },
    /**
     * 搜索框形状：默认正方矩形/半圆形（square/round）
     */
    shape: {
      type: String,
      value: 'square'
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    /**
     * 取消图标是否显示
     */
    cancelIconShow: false,
    /**
     * 搜索值
     */
    searchValue: '',
  },

  /**
   * 组件的方法列表
   */
  methods: {
    /**
     * 若木搜索组件输入框键盘输入事件
     * @param {*} e
     */
    ruomuSearchKeyboardInput(e) {
      if (e.detail.value !== '') {
        this.setData({
          cancelIconShow: true
        })
      } else {
        this.setData({
          cancelIconShow: false
        })
      }
    },
    /**
     * 若木搜索组件输入框聚焦事件
     * @param {*} e
     */
    ruomuSearchFocus(e) {
      if (e.detail.value !== '') {
        this.setData({
          cancelIconShow: true
        })
      }
    },
    /**
     * 若木搜索组件输入框失去焦点事件
     * @param {*} e
     */
    ruomuSearchBlur(e) {
      if (e.detail.value === '') {
        this.setData({
          cancelIconShow: false
        })
      } else {
        this.setData({
          cancelIconShow: false
        })
      }
    },
    /**
     * 若木搜索组件输入框确认事件
     * @param {*} e
     */
    ruomuSearchConfirm(e) {
      this.triggerEvent(e.target.dataset.ruomuFuncSearch, e.detail)
    },
    /**
     * 若木搜索组件输入框清除事件
     * @param {*} e
     */
    ruomuSearchClear(e) {
      console.log(e)
      console.log('ruomuSearchClear')
      this.setData({
        searchValue: ''
      })
    },
    /**
     * 若木搜索组件输入框右侧图标取消事件
     * @param {*} e
     */
    ruomuSearchCancelTab(e) {
      console.log(e)
      console.log('ruomuSearchCancelTab')
    }
  }
})

// src/ruomu-icon.js
Component({
  /**
   * 组件希望接受外部传入的样式类。此时可以在 Component 中用 externalClasses 定义段定义若干个外部样式类。
   */
  externalClasses: ['ruomu-class'],
  options: {
    addGlobalClass: !0
  },
  /**
   * 组件的属性列表
   */
  properties: {
    /**
     * 图标名称
     */
    iconName: {
      type: String,
      value: '',
    },
    size: {
      type: Number,
      value: 38
    },
    color: {
      type: String,
      value: '#333333'
    }
  },
  /**
   * 组件的初始数据
   */
  data: {}
})
